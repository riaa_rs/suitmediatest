package com.example.rrsari.suitmediatest.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.rrsari.suitmediatest.R;
import com.example.rrsari.suitmediatest.ui.model.EventDto;
import com.example.rrsari.suitmediatest.ui.ui.Event;

import java.util.ArrayList;

/**
 * Created by rrsari on 4/18/2016.
 */
public class EventAdapter extends BaseAdapter {
    View vi;
    Event context;
    ArrayList data;
    Resources res;
    EventDto tempValues = null;
    private static LayoutInflater inflater = null;
    public EventAdapter(Event a, ArrayList d, Resources resLocal) {
        this.context = a;
        this.data = d;
        this.res = resLocal;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        if(data.size()<0) {
            return 1;
        }
        else{
            return data.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        vi = convertView;
        holder = new ViewHolder();

        if (convertView == null) {
            vi = inflater.inflate(R.layout.content_list_event, null);
            holder.event = (TextView) vi.findViewById(R.id.tvEventName);
            holder.image = (ImageView) vi.findViewById(R.id.ivEventImage);
            holder.tanggal = (TextView) vi.findViewById(R.id.tvEventDate);
            holder.position = position;
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }
        tempValues = (EventDto) data.get(position);
        drawEventData(holder,vi,tempValues,position);
        vi.setOnClickListener(new OnItemClickListener(position));
        return vi;
    }

    public static class ViewHolder {
        public TextView event;
        public ImageView image;
        public TextView tanggal;
        public int position;

    }

    private void drawEventData(ViewHolder holder, View view,EventDto data,int position){
        holder.event.setText(data.getName());
        holder.tanggal.setText(data.getTanggal());
        holder.image.setImageResource(data.getImage());

    }

    public class OnItemClickListener implements View.OnClickListener {
        private int mPosition;

        OnItemClickListener(int position) {
            mPosition = position;
        }


        @Override
        public void onClick(View arg0) {
            Event event = context;
            ViewHolder holder = (ViewHolder) arg0.getTag();
            event.onItemClickList(mPosition);
            notifyDataSetChanged();

        }
    }


}
