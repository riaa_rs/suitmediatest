package com.example.rrsari.suitmediatest.ui.service.http;

import android.content.Context;

import com.example.rrsari.suitmediatest.ui.interfaces.ServiceCallback;

public abstract class AbstractService {

    protected int callerId;
    protected int tagId;

    protected Context context;
    protected ServiceCallback serviceCallback;
    protected HttpAsyncTask httpAsyncTask;
}
