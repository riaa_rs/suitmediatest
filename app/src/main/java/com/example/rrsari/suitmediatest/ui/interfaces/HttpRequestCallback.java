package com.example.rrsari.suitmediatest.ui.interfaces;

/**
 * Created by Kartuku Digital Wallet Team
 *
 * @author: mobile.apps@kartuku.co.id
 * @since: 10/29/15 - 9:59 AM
 * Proj: DigitalWalletWidget
 * @version:
 */
public interface HttpRequestCallback {
    void preExecute();

    void postExecute(Object response);
}
