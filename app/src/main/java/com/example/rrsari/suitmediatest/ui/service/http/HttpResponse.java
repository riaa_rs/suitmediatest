package com.example.rrsari.suitmediatest.ui.service.http;

import android.content.Context;
import android.widget.Toast;

public class HttpResponse {
    private int statusCode;
    private String message;

    public static void handlingHttpResponse(Context context, HttpResponse response) {
        if (response.getMessage() != null)
            Toast.makeText(context, response.getMessage(), Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(context, "Network connection failed to response properly.", Toast.LENGTH_SHORT);
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
