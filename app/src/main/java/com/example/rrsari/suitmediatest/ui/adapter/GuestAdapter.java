package com.example.rrsari.suitmediatest.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rrsari.suitmediatest.R;
import com.example.rrsari.suitmediatest.ui.model.EventDto;
import com.example.rrsari.suitmediatest.ui.model.GuestDto;
import com.example.rrsari.suitmediatest.ui.ui.Event;
import com.example.rrsari.suitmediatest.ui.ui.Guest;

import java.util.ArrayList;

/**
 * Created by rrsari on 4/18/2016.
 */
public class GuestAdapter extends BaseAdapter {
    View vi;
    Guest context;
    ArrayList data;
    Resources res;
    GuestDto tempValues = null;
    private static LayoutInflater inflater = null;

    public GuestAdapter(Guest a, ArrayList d, Resources resLocal) {
        this.context = a;
        this.data = d;
        this.res = resLocal;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (data.size() < 0) {
            return 1;
        } else {
            return data.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        vi = convertView;
        holder = new ViewHolder();

        if (convertView == null) {
            vi = inflater.inflate(R.layout.content_list_guest, null);
            holder.image = (ImageView) vi.findViewById(R.id.ivUser);
            holder.guestName=(TextView)vi.findViewById(R.id.tvGuestName);
            holder.position = position;
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }
        tempValues = (GuestDto) data.get(position);
        drawGuestData(holder, vi, tempValues, position);
        vi.setOnClickListener(new OnItemClickListener(position));
        return vi;

    }

    public static class ViewHolder {
        public ImageView image;
        public int position;
        public TextView guestName;
    }

    private void drawGuestData(ViewHolder holder, View view, GuestDto data, int position) {
        holder.image.setImageResource(data.getProfile());
        holder.guestName.setText(data.getName());

    }

    public class OnItemClickListener implements View.OnClickListener {
        private int mPosition;

        OnItemClickListener(int position) {
            mPosition = position;
        }


        @Override
        public void onClick(View arg0) {
            Guest guest = context;
            ViewHolder holder = (ViewHolder) arg0.getTag();
            guest.onItemClickList(mPosition);
            notifyDataSetChanged();

        }

    }
}
