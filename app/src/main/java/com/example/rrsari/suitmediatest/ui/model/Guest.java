package com.example.rrsari.suitmediatest.ui.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by rrsari on 4/19/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Guest {
    int id;
    String name;
    String birthdate;
    int profile;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public int getProfile() {
        return profile;
    }

    public void setProfile(int profile) {
        this.profile = profile;
    }
}
