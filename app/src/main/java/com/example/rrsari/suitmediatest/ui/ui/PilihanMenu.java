package com.example.rrsari.suitmediatest.ui.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.rrsari.suitmediatest.R;

/**
 * Created by rrsari on 4/18/2016.
 */
public class PilihanMenu extends Activity {
    TextView tvName;
    Button btnPilihEvent;
    Button btnPilihGuest;
    String name;
    String guest;
    String event;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        bindObject();
        name=getIntent().getStringExtra("name");
        tvName.setText(name);
        event=getIntent().getStringExtra("event");
        guest=getIntent().getStringExtra("guest");
        if(event!=null) {
            btnPilihEvent.setText(event);
        }
        if(guest!=null) {
            btnPilihGuest.setText(guest);
        }

    }

    private void bindObject(){
        tvName=(TextView)findViewById(R.id.tvName);
        btnPilihEvent=(Button)findViewById(R.id.btnPilihEvent);
        btnPilihGuest=(Button)findViewById(R.id.btnPilihGuest);
        findViewById(R.id.btnPilihEvent).setOnClickListener(btnEventOnClickListener);
        findViewById(R.id.btnPilihGuest).setOnClickListener(btnGuestOnClickListener);
    }

    private View.OnClickListener btnEventOnClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent event =new Intent(v.getContext(),Event.class);
            event.putExtra("name",name);
            event.putExtra("guest",guest);
            startActivity(event);
        }
    };

    private View.OnClickListener btnGuestOnClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent guest =new Intent(v.getContext(),Guest.class);
            guest.putExtra("name", name);
            guest.putExtra("event",event);
            startActivity(guest);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        //getActionBar().setIcon(R.mipmap.back_arrow);
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        View mActionBarView = getLayoutInflater().inflate(R.layout.my_action_bar, null);
        actionBar.setCustomView(mActionBarView);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity_home in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
