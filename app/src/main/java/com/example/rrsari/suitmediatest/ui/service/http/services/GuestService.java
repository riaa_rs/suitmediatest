package com.example.rrsari.suitmediatest.ui.service.http.services;

import android.content.Context;

import com.example.rrsari.suitmediatest.ui.constant.Constants;
import com.example.rrsari.suitmediatest.ui.interfaces.HttpRequestCallback;
import com.example.rrsari.suitmediatest.ui.interfaces.ServiceCallback;
import com.example.rrsari.suitmediatest.ui.model.GuestDto;
import com.example.rrsari.suitmediatest.ui.model.ListGuestDto;
import com.example.rrsari.suitmediatest.ui.service.http.AbstractService;
import com.example.rrsari.suitmediatest.ui.service.http.HttpAsyncTask;

import org.springframework.http.HttpMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rrsari on 4/18/2016.
 */
public class GuestService extends AbstractService implements HttpRequestCallback {
    private static int CALLER_ID;
    private static int TAG_ID;
    private ServiceCallback serviceCallback;

    public GuestService(Context context, int tag, int id) {
        CALLER_ID = id;
        TAG_ID = tag;
        this.context = context;
        this.serviceCallback=(ServiceCallback)this.context;
    }

    public void serviceGuest(){
        HttpAsyncTask task = new HttpAsyncTask(
                Constants.URL_ROOT,
                null,
                GuestDto[].class,
                this,
                HttpMethod.GET);
        task.setRequestTimeout(Constants.HTTP_TASK_REQ_TIMEOUT);
        task.setWithHeader(false, this.context);
        task.execute();

    }

    @Override
    public void preExecute() {

    }

    @Override
    public void postExecute(Object response) {
        this.serviceCallback.postServiceExecute(response, 0x00000002, 0);
    }
}
