package com.example.rrsari.suitmediatest.ui.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.Deserializers;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rrsari on 4/19/2016.
 */
@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class ListGuestDto{
    private List<GuestDto> guestList;

    public ListGuestDto(ListGuestDto copyObject) {
        this.guestList.addAll(copyObject.getGuestList());
    }
    public ListGuestDto() {
        this.guestList = new ArrayList();
    }

    public List<GuestDto> getGuestList() {
        return guestList;
    }


    public void setGuestList(List<GuestDto> guestList) {
        this.guestList = guestList;
    }
}
