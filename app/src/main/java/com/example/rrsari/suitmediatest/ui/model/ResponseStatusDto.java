package com.example.rrsari.suitmediatest.ui.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by ddiputra@kartuku.co.id on 5/27/2015.
 *
 * @author R&D PT. Multi Adiprakarsa Manunggal (Kartuku)
 * @version 1.0
 * @since 5/27/2015
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseStatusDto {

    private String code;
    private String message;
    private String detail;

    public ResponseStatusDto() {
    }

    public ResponseStatusDto(String detail) {
        this.detail = detail;
    }

    public ResponseStatusDto(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public ResponseStatusDto(String code, String message, String detail) {
        this.code = code;
        this.message = message;
        this.detail = detail;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
