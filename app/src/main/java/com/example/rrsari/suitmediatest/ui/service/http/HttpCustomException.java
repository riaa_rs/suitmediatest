package com.example.rrsari.suitmediatest.ui.service.http;

import org.springframework.http.HttpStatus;

import java.io.IOException;


public class HttpCustomException extends IOException {

    private HttpStatus statusCode;

    private String body;

    public HttpCustomException(String msg) {
        super(msg);
    }

    public HttpCustomException(HttpStatus statusCode, String body, String msg) {
        super(msg);
        this.statusCode = statusCode;
        this.body = body;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

}

