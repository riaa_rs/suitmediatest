package com.example.rrsari.suitmediatest.ui.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.example.rrsari.suitmediatest.R;

public class Home extends AppCompatActivity {
    TextView etName;
    Button btnNext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        bindObject();
        findViewById(R.id.btnNextHome).setOnClickListener(nextHomeOnClickListener);
    }

    private void bindObject(){
        etName=(TextView)findViewById(R.id.etNama);
        btnNext=(Button)findViewById(R.id.btnNextHome);
    }

    private View.OnClickListener nextHomeOnClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i=new Intent(v.getContext(),PilihanMenu.class);
            i.putExtra("name",etName.getText().toString());
            startActivity(i);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity_home in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
