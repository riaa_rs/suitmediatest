package com.example.rrsari.suitmediatest.ui.interfaces;

/**
 * @author Dandi Diputra <ddiputra@kartuku.co.id>
 * @version 0.1
 * @since 2015-05-05
 */
public interface ServiceCallback {
    void preServiceExecute();

    void postServiceExecute(Object response, int tag, int callerId);
}
