package com.example.rrsari.suitmediatest.ui.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.rrsari.suitmediatest.R;
import com.example.rrsari.suitmediatest.ui.adapter.EventAdapter;
import com.example.rrsari.suitmediatest.ui.adapter.GuestAdapter;
import com.example.rrsari.suitmediatest.ui.interfaces.ServiceCallback;
import com.example.rrsari.suitmediatest.ui.model.EventDto;
import com.example.rrsari.suitmediatest.ui.model.GuestDto;
import com.example.rrsari.suitmediatest.ui.model.ListGuestDto;
import com.example.rrsari.suitmediatest.ui.service.http.services.GuestService;

import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by rrsari on 4/18/2016.
 */
public class Guest extends Activity implements ServiceCallback {
    private ArrayList<GuestDto> mListViewGuestAdapter = new ArrayList<GuestDto>();
    GridView gridViewGuest;
    GuestAdapter adapter;
    ArrayList<Integer> profileGuest;
    GuestService guestService;
    GuestDto[] guestDto;
    GuestDto guest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest);
        guestService=new GuestService(this,0x00000002,0);
        guestService.serviceGuest();
        bindObject();
        showGuest();
    }

    private void bindObject() {
        gridViewGuest = (GridView) findViewById(R.id.gridViewGuest);

        profileGuest=new ArrayList<Integer>(){{
            add(R.drawable.ic_profile);
            add(R.drawable.ic_profile);
            add(R.drawable.ic_profile);
            add(R.drawable.ic_profile);
            add(R.drawable.ic_profile);
        }};
    }

    public void onItemClickList(int mPosition) {
        guest=mListViewGuestAdapter.get(mPosition);
        Intent i=new Intent(this, PilihanMenu.class);
        i.putExtra("name", getIntent().getStringExtra("name"));
        i.putExtra("guest", guest.getName());
        i.putExtra("event", getIntent().getStringExtra("event"));
        String tanggal=guest.getBirthdate().substring(8,10);
        Log.d("tanggal", tanggal);
        Toast.makeText(this,birthdateGift(),Toast.LENGTH_LONG).show();
        startActivity(i);
    }

    private void showGuest(){
        adapter = new GuestAdapter(this, mListViewGuestAdapter, getResources());
        gridViewGuest.setAdapter(adapter);

    }

    private String birthdateGift(){
        int tanggal=Integer.parseInt(guest.getBirthdate().substring(8, 10));
        String gift;
        if(tanggal%2==0 && tanggal%3==0)
        {
            gift="iOS";
        }
        else if(tanggal%2==0){
            gift="Blackberry";
        }
        else if(tanggal%3==0){
            gift="Android";
        }
        else{
            gift="Phone";
        }

        return gift;


    }

    private void setListData() {
        for (int i = 0; i < guestDto.length; i++) {
            GuestDto guestDtos = new GuestDto();
            guestDtos.setName(Arrays.asList(guestDto).get(i).getName());
            guestDtos.setBirthdate(Arrays.asList(guestDto).get(i).getBirthdate());
            guestDtos.setId(Arrays.asList(guestDto).get(i).getId());
            guestDtos.setProfile(profileGuest.get(i));
            mListViewGuestAdapter.add(guestDtos);
        }
    }

    @Override
    public void preServiceExecute() {

    }

    @Override
    public void postServiceExecute(Object response, int tag, int callerId) {
        if (response instanceof ResponseEntity) {
            if (((ResponseEntity) response).getBody() instanceof GuestDto[]) {
                guestDto= ((ResponseEntity<GuestDto[]>) response).getBody();
                    setListData();
                    showGuest();

            }

        }
    }
}

