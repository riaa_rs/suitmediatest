package com.example.rrsari.suitmediatest.ui.model;

/**
 * Created by rrsari on 4/18/2016.
 */
public class EventDto {
    String name;
    String tanggal;
    int image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
