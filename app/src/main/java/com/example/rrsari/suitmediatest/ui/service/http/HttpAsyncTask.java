package com.example.rrsari.suitmediatest.ui.service.http;

import android.content.Context;
import android.os.AsyncTask;

import com.example.rrsari.suitmediatest.ui.constant.Constants;
import com.example.rrsari.suitmediatest.ui.interfaces.HttpRequestCallback;
import com.example.rrsari.suitmediatest.ui.model.GuestDto;
import com.example.rrsari.suitmediatest.ui.model.ListGuestDto;
import com.example.rrsari.suitmediatest.ui.model.ResponseStatusDto;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;


public class HttpAsyncTask extends AsyncTask<Void, Void, Object> {

    private final String url;
    private final Object body;
    private final HttpRequestCallback restCallback;
    private final Class<?> responseType;
    private Context context;
    private boolean withHeader = false;
    private HttpMethod method;
    private int requestTimeout;


    public HttpAsyncTask(String url, Object body, Class<?> responseType,
                         HttpRequestCallback restCallback, HttpMethod method) {
        this.url = url;
        this.body = body;
        this.method = method;
        this.restCallback = restCallback;
        this.responseType = responseType;
        this.requestTimeout = 15;

    }

    public void setRequestTimeout(int requestTimeout) {
        this.requestTimeout = requestTimeout;
    }

    public boolean isWithHeader() {
        return withHeader;
    }

    public void setWithHeader(boolean withHeader, Context context) {
        this.withHeader = withHeader;
        this.context = context;
    }

    private ClientHttpRequestFactory clientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setReadTimeout(this.requestTimeout);
        factory.setConnectTimeout(this.requestTimeout);
        return factory;
    }

    @Override
    protected Object doInBackground(Void... params) {
        HttpEntity<?> request = null;

        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory());
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//        objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(new ListGuestDto(List<GuestDto>));
        MappingJackson2HttpMessageConverter mappingJackson2 = new MappingJackson2HttpMessageConverter();
        mappingJackson2.setObjectMapper(objectMapper);
        messageConverters.add(mappingJackson2);
        //messageConverters.add(new MappingJacksonHttpMessageConverter());
        restTemplate.setMessageConverters(messageConverters);
        restTemplate.setErrorHandler(new HttpCustomResponseErrorHandler());

        if (isWithHeader()) {

            MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
//            headers.add("Content-Type", "application/json");
//            headers.add("UID",this.uid);
//            headers.add("REQUEST-TYPE", "ENC");
//            headers.add("DEVICE-ID",this.deviceId);

            request = new HttpEntity<>(body, headers);
        }

        try {
            if (method == HttpMethod.GET) {
                return restTemplate.exchange(url, HttpMethod.GET, request, responseType);
            } else if (method == HttpMethod.POST) {
                if (request != null) {
                    return restTemplate.postForEntity(url, request, responseType);
                } else {
                    return restTemplate.postForEntity(url, body, responseType);
                }
            }
        } catch (Exception e) {
            return parsingException(e.getCause().getMessage());
        }
        return null;
    }

    private ResponseStatusDto parsingException(String data) {
        ResponseStatusDto response = new ResponseStatusDto();
        JSONObject jsonObject;
        String statusCode;
        String message;

        try {
            if (data != null) {
                jsonObject = new JSONObject(data);
            } else {
                return response;
            }
            statusCode = jsonObject.getString(Constants.RESPONSE_CODE_KEY);
            message = jsonObject.getString(Constants.RESPONSE_MESSAGE_KEY);
            response.setMessage(message);
            response.setCode(statusCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onPreExecute() {
        restCallback.preExecute();
    }

    @Override
    protected void onPostExecute(Object response) {
        restCallback.postExecute(response);
    }
}
