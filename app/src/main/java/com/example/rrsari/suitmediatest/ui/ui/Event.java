package com.example.rrsari.suitmediatest.ui.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ListView;

import com.example.rrsari.suitmediatest.R;
import com.example.rrsari.suitmediatest.ui.adapter.EventAdapter;
import com.example.rrsari.suitmediatest.ui.model.EventDto;

import java.util.ArrayList;

/**
 * Created by rrsari on 4/18/2016.
 */
public class Event extends Activity {
    private ArrayList<EventDto> mListViewEventAdapter = new ArrayList<EventDto>();
    ListView listViewEvent;
    EventAdapter adapter;
    ArrayList<String> nameEvent;
    ArrayList<String> tanggalEvent;
    ArrayList<Integer> imageEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        bindObject();
        setListData();
        showEvent();
       }
    private void bindObject(){
        listViewEvent=(ListView)findViewById(R.id.listViewEvent);
        nameEvent=new ArrayList<String>(){{
            add("Alfamart Show");
            add("HardRock Event");
            add("ALLEIRA Night");
            add("Happy 100 years McD");
        }};

        tanggalEvent=new ArrayList<String>(){{
            add("20 April 2016");
            add("1 Mei 2016");
            add("17 Agustus 2016");
            add("25 September 2016");
        }};

        imageEvent=new ArrayList<Integer>(){{
            add(R.drawable.event_01);
            add(R.drawable.event_02);
            add(R.drawable.event_03);
            add(R.drawable.event_04);
        }};


    }
    private void showEvent(){
        adapter = new EventAdapter(this, mListViewEventAdapter, getResources());
        listViewEvent.setAdapter(adapter);

    }
    public void onItemClickList(int mPosition) {
        EventDto eventDto=mListViewEventAdapter.get(mPosition);
        Intent i=new Intent(this, PilihanMenu.class);
        i.putExtra("event",eventDto.getName());
        i.putExtra("name",getIntent().getStringExtra("name"));
        i.putExtra("guest",getIntent().getStringExtra("guest"));
        startActivity(i);
    }
    private void setListData() {
        for (int i = 0; i < 4; i++) {
                EventDto eventDto = new EventDto();
                eventDto.setName(nameEvent.get(i));
                eventDto.setTanggal(tanggalEvent.get(i));
                eventDto.setImage(imageEvent.get(i));
                mListViewEventAdapter.add(eventDto);

        }
    }


}
