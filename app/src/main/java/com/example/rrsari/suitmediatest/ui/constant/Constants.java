package com.example.rrsari.suitmediatest.ui.constant;

/**
 * Created by rrsari on 4/18/2016.
 */
public class Constants {
    public static final String URL_ROOT="http://dry-sierra-6832.herokuapp.com/api/people";
    public static final String RESPONSE_CODE_KEY = "code";
    public static final String RESPONSE_MESSAGE_KEY = "message";
    public static final int HTTP_TASK_REQ_TIMEOUT = 30000;
}
