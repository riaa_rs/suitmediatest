package com.example.rrsari.suitmediatest.ui.service.http;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class HttpCustomResponseErrorHandler implements ResponseErrorHandler {

    public static boolean isError(HttpStatus status) {
        HttpStatus.Series series = status.series();
        return (HttpStatus.Series.CLIENT_ERROR.equals(series)
                || HttpStatus.Series.SERVER_ERROR.equals(series));
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        String body = convertInputStreamToString(response.getBody());
        HttpCustomException exception = new HttpCustomException(response.getStatusCode(), body, body);
        throw exception;
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line);
        }
        return total.toString();
    }

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return this.isError(response.getStatusCode());
    }
}